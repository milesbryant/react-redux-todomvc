import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import App from './components/App';
import todoApp from './shared/reducers';

let store = createStore(todoApp);




render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('app')
);
