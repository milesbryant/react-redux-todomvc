import { Component } from 'react';
import AddTodo from './containers/AddTodo';

const TodoHeader = () => (
      <header class="header">
				<h1>todos</h1>
        <AddTodo />
			</header>
    );

export default TodoHeader;
