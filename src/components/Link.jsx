import { PropTypes } from 'react';
import classNames from 'classnames';

const Link = ({ selected, children, onClick }) => {
  return (
    <a href="#"
        className={classNames({'selected': selected})}
       onClick={e => {
         e.preventDefault()
         onClick()
       }}
    >
      {children}
    </a>
  );
};

Link.propTypes = {
  selected: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired,
  onClick: PropTypes.func.isRequired
};

export default Link;
