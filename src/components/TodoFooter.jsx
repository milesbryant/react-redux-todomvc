import { FilterLink, RemoveAllLink } from './containers/Links';
import ItemsLeftCounter from './containers/ItemsLeftCounter';
import { VisibilityFilters } from '../shared/actions';

const TodoFooter = ({filterAll, filterActive, filterCompleted}) => (
      <footer className="footer">
				<ItemsLeftCounter />
				<ul className="filters">
					<li>
						<FilterLink filter={VisibilityFilters.SHOW_ALL}>All</FilterLink>
					</li>
					<li>
						<FilterLink filter={VisibilityFilters.SHOW_ACTIVE}>Active</FilterLink>
					</li>
					<li>
						<FilterLink filter={VisibilityFilters.SHOW_COMPLETED}>Completed</FilterLink>
					</li>
				</ul>
				<RemoveAllLink><button className="clear-completed">Clear completed</button></RemoveAllLink>
			</footer>
    );


export default TodoFooter;
