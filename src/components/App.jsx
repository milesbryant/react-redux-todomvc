import { Component } from 'react';

import VisibleTodoList from './containers/VisibleTodoList';
import TodoHeader from './TodoHeader';
import TodoFooter from './TodoFooter';

export default class App extends Component {
  render() {
    return (
  		<section className="todoapp" id="app">
        <TodoHeader />
        <VisibleTodoList />
        <TodoFooter />
      </section>
    );
  }
}
