import { connect } from 'react-redux';
import { VisibilityFilters, toggleTodo, removeTodo } from '../../shared/actions';
import TodoList from '../TodoList';

const getVisibleTodos = (todos, filter) => {
  switch (filter) {
    case VisibilityFilters.SHOW_ALL:
      return todos;
    case VisibilityFilters.SHOW_COMPLETED:
      return todos.filter(t => t.completed);
    case VisibilityFilters.SHOW_ACTIVE:
      return todos.filter(t => !t.completed);
  }
}

const mapStateToProps = (state) => {
  return {
    todos: getVisibleTodos(state.todos, state.visibilityFilter)
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    onTodoToggle: (id) => dispatch(toggleTodo(id)),
    onDelete: (id) => dispatch(removeTodo(id))
  };
}

const VisibleTodoList = connect(
  mapStateToProps,
  mapDispatchToProps
)(TodoList);

export default VisibleTodoList;
