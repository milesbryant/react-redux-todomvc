import { connect } from 'react-redux';

let ItemsLeftCounter = ({numActive}) => (
  <span className="todo-count"><strong>{numActive}</strong> item{numActive==1 ? '' : 's'}   left
  </span>
);

const mapStateToProps = (state) => {
  return {
    numActive: state.todos.filter(t => !t.completed).length
  };
}

export default connect(mapStateToProps)(ItemsLeftCounter);
