import { connect } from 'react-redux';
import { setFilter, toggleAll, removeAll } from '../../shared/actions';

import Link from '../Link';

const createLink = (selectedCallback, dispatchCallback) =>
  connect(
    (state, ownProps) => { return { selected: selectedCallback(state, ownProps) } },
    (dispatch, ownProps) => { return { onClick: () => dispatch(dispatchCallback(ownProps)) } }
  )(Link);

export const FilterLink = createLink(
  (state, ownProps) => (ownProps.filter === state.visibilityFilter),
  (ownProps) => setFilter(ownProps.filter)
);

export const ToggleAllLink = createLink(
  () => false,
  toggleAll
);

export const RemoveAllLink = createLink(
  () => false,
  removeAll
);
