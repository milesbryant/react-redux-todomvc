import { connect } from 'react-redux';
import { addTodo } from '../../shared/actions';

let AddTodo = ({ dispatch }) => {
  let input;
  return (
    <form onSubmit={e => {
        e.preventDefault();
        if(!input.value.trim()) {
          return;
        }
        dispatch(addTodo(input.value));
        input.value = '';
      }}
    >
        <input
          className="new-todo"
          placeholder="What needs to be done?"
          autofocus
          ref={node => {input = node;}}
        />
    </form>
  )
};

AddTodo = connect()(AddTodo);

export default AddTodo;
