import classNames from 'classnames';
import { PropTypes } from 'react';


const Todo = ({ onToggleComplete, onDelete, onClick, completed, editing, text }) => {
  var todoClass = classNames({
    'completed': completed, 'editing': editing
  });
  return (
    <li className={todoClass}>
      <div className="view">
        <input className="toggle" type="checkbox" onChange={onToggleComplete} checked={completed}/>
        <label onClick={onClick}>{text}</label>
        <button className="destroy" onClick={onDelete}></button>
      </div>
      <input className="edit" />
    </li>
)};

Todo.propTypes = {
  onToggleComplete: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  onClick: PropTypes.func,
  completed: PropTypes.bool,
  editing: PropTypes.bool,
  text: PropTypes.string
}

export default Todo;
