import { PropTypes } from 'react';
import { ToggleAllLink } from './containers/Links';
import Todo from './Todo';

const TodoList = ({ todos, onTodoToggle, onDelete }) => {
  return (
    <section className="main">
      <ToggleAllLink><input className="toggle-all" type="checkbox" /></ToggleAllLink>
      <ul className="todo-list">
        {todos.map(todo => (
          <Todo
            key={todo.id}
            {...todo}
            onToggleComplete={() => onTodoToggle(todo.id)}
            onDelete={() => onDelete(todo.id)}
          />
        ))}
      </ul>
    </section>
  );
}

TodoList.propTypes = {
  todos: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    completed: PropTypes.bool.isRequired,
    text: PropTypes.string.isRequired
  })),
  onTodoToggle: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired
}

export default TodoList;
