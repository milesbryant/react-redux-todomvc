'use strict';

import * as actions from './actions';
import { combineReducers } from 'redux';

const initialState = {
  filter: actions.VisibilityFilters.SHOW_ALL,
  todos: [
    {
      id: 0,
      text: 'Begin learning React',
      completed: true
    },
    {
      id: 1,
      text: 'Learn React',
      completed: false
    },
    {
      id: 2,
      text: 'Master React',
      completed: false
    }
  ]
};

const setCompleted = (state, newCompletedState) => {
  return Object.assign({}, state, {
    completed: newCompletedState
  });
};

const todo = (state, action) => {
  switch (action.type) {
    case actions.ADD_TODO:
      return {
        id: action.id,
        text: action.text,
        completed: false
      };
    case actions.TOGGLE_TODO:
      if (state.id !== action.id) {
        return state;
      }
      return setCompleted(state, !state.completed);
    default:
      return state;
  }
};

const todos = (state = initialState.todos, action) => {
  switch (action.type) {
    case actions.ADD_TODO:
      return [
        ...state,
        todo(undefined, action)
      ];
    case actions.TOGGLE_TODO:
      return state.map(t => todo(t, action));
    case actions.REMOVE_TODO:
      return state.filter(todo => todo.id !== action.id);
    case actions.TOGGLE_ALL:
      var numCompleted = state.filter(t => t.completed).length;
      return state.map(t => setCompleted(t, numCompleted !== state.length));
    case actions.REMOVE_ALL:
      return state.filter(t => !t.completed);
    default:
      return state;

  }

};

const visibilityFilter = (state = initialState.filter, action) => {
  switch (action.type) {
    case actions.SET_FILTER:
      return action.filter;
    default:
      return state;
  }
}

const todoApp = combineReducers({
  visibilityFilter,
  todos
});

export default todoApp;
