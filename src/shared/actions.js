
export const ADD_TODO = 'ADD_TODO';
export const REMOVE_TODO = 'REMOVE_TODO';
export const REMOVE_ALL = 'REMOVE_ALL';
export const TOGGLE_TODO = 'TOGGLE_TODO';
export const TOGGLE_ALL = 'TOGGLE_ALL';
export const EDIT_TODO = 'EDIT_TODO';
export const SET_FILTER = 'SET_FILTER';

export const VisibilityFilters = {
  SHOW_ALL: 'SHOW_ALL',
  SHOW_COMPLETED: 'SHOW_COMPLETED',
  SHOW_ACTIVE: 'SHOW_ACTIVE'
}

let nextTodoId = 0;

const createAction = (type, id) => {
  return {
    type: type,
    id: id
  };
}

export const toggleAll = () => createAction(TOGGLE_ALL);
export const removeAll = () => createAction(REMOVE_ALL);
export const removeTodo = (id) => createAction(REMOVE_TODO, id);
export const toggleTodo = (id) => createAction(TOGGLE_TODO, id);
export const editTodo = (id) => createAction(EDIT_TODO, id);

export const addTodo = (text) => {
  return {
    type: ADD_TODO,
    id: nextTodoId++,
    text
  };
};
export const setFilter = (filter) => {
  return {
    type: SET_FILTER,
    filter: filter
  };
};
