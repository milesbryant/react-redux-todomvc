'use strict';

import gulp from 'gulp';
import * as bs from 'browser-sync';
import babel from 'gulp-babel';
import webpack from 'webpack-stream';
import webpackConfig from './webpack.config.babel';
import path from 'path';

const browserSync = bs.create();

gulp.task('copy-html', () => {
  return gulp.src('src/**/*.html')
    .pipe(gulp.dest('.tmp/serve'));
});

gulp.task('webpack', () => {
  return gulp.src('src/index.jsx')
    .pipe(webpack({
      watch: false,
    	output: {
    		path: path.join(__dirname, '.tmp/build'),
    		filename: 'bundle.js'
    	},
      module: {
        loaders: [
          {
            test: /\.jsx?/,
            exclude: /(node_modules|bower_components)/,
            loader: 'babel-loader',
            query: {
              presets: ['es2015', 'react']
            }
          }
        ]
      },
      externals: {
        "react": "React",
        "react-dom": "ReactDOM"
      },
      resolve: {
        extensions: [
          "", ".js", ".jsx"
        ]
      },
      devtool: 'inline-source-map'
    }))
    .pipe(gulp.dest('./.tmp/serve/'))
});

gulp.task('copy-serve', () => {
  return gulp.src('./.tmp/build/**/*')
    .pipe(gulp.dest('./.tmp/serve'));
})

gulp.task('reload', ['copy-html', 'webpack'], () => {
  browserSync.reload();
});

gulp.task('serve', ['copy-html', 'webpack'], () => {
  browserSync.init({
    server: {
      baseDir: './.tmp/serve',
      routes: {
        '/bower_components': 'bower_components',
        '/node_modules': 'node_modules'
      }
    }
  });

  gulp.watch('./src/**/*', ['reload']);
});
