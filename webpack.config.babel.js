import path from 'path';

module.exports = {
	entry: {
	  preload: './.tmp/src/index.js'
	},
	output: {
		path: path.join(__dirname, '.tmp/build'),
		filename: 'bundle.js'
	}
};
